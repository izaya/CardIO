﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameOver : MonoBehaviour {
	private GameObject messageUI = null;

	void Awake () {
		messageUI = gameObject.transform.GetChild(0).gameObject;
	}

	public void updateText (string _text) {
		messageUI.GetComponent<Text>().text = _text;
	}
}

