﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Hand : MonoBehaviour {
	public static Hand instance = null;
	public Card cardPrefab;

	private int userId;
	private int state;
	private int price;
	private Dictionary<int, Card> cards = new Dictionary<int, Card> ();
	private Dictionary<int, Card> playedCards = new Dictionary<int, Card> ();
	private int attackableCount;

	const int ONLINE = 0;
	const int CONNECTED = 1;
	const int ENEMYTURN = 2;
	const int MYTURN = 3;
	const int WIN = 4;
	const int LOSE = 5;
	const int OFFLINE = -1;

	const int INDECK = 0;
	const int INHAND1 = 1;      // in hand, unable to play
	const int INHAND2 = 2;      // in hand, able to play
	const int ONTABLE1 = 3;     // on table, unable to attack
	const int ONTABLE2 = 4;     // on table, able to attack
	const int DESTROIED = -1;

	void Awake () {
		//Check if there is already an instance of Hand.
		if (instance == null) {
			//if not, set it to this.
			instance = this;
			//If instance already exists:
		} else if (instance != this) {
			//Destroy this, this enforces our singleton pattern so there can only be one instance of Hand.
			Destroy (gameObject);
		}
		//Set Deck to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
		DontDestroyOnLoad (gameObject);

		price = 1;
		attackableCount = 0;
		state = OFFLINE;
	}

	public void init (int _userId, int _state, List<Card> _cards) {
		userId = _userId;
		state = _state;

		foreach (Card _card in _cards) {
			cards.Add (_card.getId (), _card);
			_card.transform.SetParent (Hand.instance.transform);
		}
	}

	public int getId () {
		return userId;
	}

	public int getSize () {
		return cards.Count;
	}		

	public int getState () {
		return state;
	}

	public int getPrice () {
		return price;
	}

	public int getAttackableCount () {
		return attackableCount; 
	}

	public void reduceAttackableCount () {
		attackableCount--;
	}

	public void setId (int _id) {
		userId = _id;
	}

	public void setState (int _state) {
		state = _state;
	}

	// reset available price when starting a new turn
	public void setPrice (int _price) {
		price = _price;
	}

	public void insert (Card item) {
		cards.Add (item.getId (), item);
		item.transform.SetParent (instance.transform);
		item.setState (INHAND1);		// state 1: in hand, unable to play
	}

	// when a card is played on table top, remove it from hand cards and add it to playedCard dictionary
	public bool play (Card item) {
		if (cards.Remove (item.getId())) {
			item.setState (ONTABLE1);
			playedCards.Add (item.getId(), item);
			return true;
		}
		Debug.Log ("Fail to remove card from hand");
		return false;
	}	

	public Card getCardById (int _id) {
		if (playedCards.ContainsKey (_id)) {
			return playedCards[_id];
		}
		Debug.Log ("Fail to find card from hand played cards");
		return null;
	}

	// Remove and destroy this card by ID;
	public bool removeCardById (int _id) {
		if (playedCards.ContainsKey (_id)) {
			Card thisCard = playedCards [_id];
			playedCards.Remove (_id);
			thisCard.updateHealth (-1);
			return true;
		}
		return false;
	}

	// Update hand and hero price
	public void updatePrice (int val) {
		price -= val;
		MyHero.instance.updatePrice (price);
	}

	// Calculate current available price and set playable cards into state 2
	// End turn turn by returning false if no cards playable
	public bool calculatePrice	() {
		if (price < 0) {
			return false;
		}
		bool findCard = false;

		foreach (var item in cards.Values) {
			if (item.getPrice () <= price) {
				item.hightlight ();		// state 2: in hand, able to play
				findCard = true;
			} else {
				item.disable ();
			}
		}
		return findCard;
	}

	// set all the cards played on table from state 3 to 4
	public void attackPrepare () {
		attackableCount = 0;

		foreach(var item in playedCards.Values) {
			item.GetComponent<Card> ().active ();
			attackableCount++;
		}
	}

	// calculate price and attack prepare;
	public void attackInit () {
		calculatePrice ();
		attackPrepare ();
	}		

	public bool isEndTurn () {
		if (!calculatePrice () && attackableCount <= 0) {
			return true;
		}
		return false;
	}
}
