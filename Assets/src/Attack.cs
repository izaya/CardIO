﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Attack : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {
	public Transform parentToReturnTo;
	public Transform placeholderParent;
	GameObject placeholder = null;

	void Awake () {
		parentToReturnTo = GameObject.Find ("Tabletop").transform;
		placeholderParent = GameObject.Find ("Tabletop").transform;
	}

	public void OnBeginDrag(PointerEventData eventData) {
		if (gameObject.GetComponent<Card>().getState() == 4 && Hand.instance.getState() == 3 ) {			
			placeholder = new GameObject();
			placeholder.transform.SetParent( this.transform.parent );
			LayoutElement le = placeholder.AddComponent<LayoutElement>();
			le.preferredWidth = this.GetComponent<LayoutElement>().preferredWidth;
			le.preferredHeight = this.GetComponent<LayoutElement>().preferredHeight;
			le.flexibleWidth = 0;
			le.flexibleHeight = 0;

			placeholder.transform.SetSiblingIndex( this.transform.GetSiblingIndex() );

			parentToReturnTo = this.transform.parent;
			placeholderParent = parentToReturnTo;
			this.transform.SetParent( this.transform.parent.parent );

			GetComponent<CanvasGroup>().blocksRaycasts = false;
		}
	}

	public void OnDrag(PointerEventData eventData) {
		if (gameObject.GetComponent<Card> ().getState () == 4 && Hand.instance.getState() == 3 ) {
			this.transform.position = eventData.position;

			if (placeholder.transform.parent != placeholderParent) {
				placeholder.transform.SetParent (placeholderParent);
			}

			int newSiblingIndex = placeholderParent.childCount;

			for (int i = 0; i < placeholderParent.childCount; i++) {
				if (this.transform.position.x < placeholderParent.GetChild (i).position.x) {
					newSiblingIndex = i;

					if (placeholder.transform.GetSiblingIndex () < newSiblingIndex) {
						newSiblingIndex--;
					}
					break;
				}
			}
			placeholder.transform.SetSiblingIndex (newSiblingIndex);
		}
	}

	public void OnEndDrag(PointerEventData eventData) {
		if (gameObject.GetComponent<Card> ().getState () == 4) {
			this.transform.SetParent (parentToReturnTo);
			this.transform.SetSiblingIndex (placeholder.transform.GetSiblingIndex ());
			GetComponent<CanvasGroup> ().blocksRaycasts = true;

			Destroy (placeholder);
			// return to state 3: on table, unable to attack after attack
			gameObject.GetComponent<Card> ().deactive ();
		}
	}
}
