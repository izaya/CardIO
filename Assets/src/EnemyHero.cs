﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnemyHero : MonoBehaviour {
	public static EnemyHero instance = null;
	private int health = 20;
	private GameObject healthUI = null;

	void Awake () {
		//Check if there is already an instance of Hand.
		if (instance == null) {
			//if not, set it to this.
			instance = this;
			//If instance already exists:
		} else if (instance != this) {
			//Destroy this, this enforces our singleton pattern so there can only be one instance of Hand.
			Destroy (gameObject);
		}
		//Set Deck to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
		DontDestroyOnLoad (gameObject);
	}

	void Start () {
		healthUI = gameObject.transform.Find ("Health").gameObject;
		healthUI.GetComponent<Text> ().text = "20";
	}

	void OnTriggerEnter2D (Collider2D other) {
		sabotage (other.gameObject.GetComponent<Card> ().getId (), other.gameObject.GetComponent<Card> ().getDamage ());
	}

	public int getHealth () {
		return health;
	}

	public void sabotage (int id, int val) {
		health -= val;
		healthUI.GetComponent<Text> ().text = health.ToString();
		GameManager.instance.emitAttackHero (id, val);
	}

	public void updateHealth (int _health) {
		health = _health;
		healthUI.GetComponent<Text> ().text = _health.ToString();
	}
}
