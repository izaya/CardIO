﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TurnBtn : MonoBehaviour {
	private bool state = false;		// indicate whose turn
	private GameObject textUI;

	public static TurnBtn instance = null;

	void Awake () {
		//Check if there is already an instance of Hand.
		if (instance == null) {
			//if not, set it to this.
			instance = this;
			//If instance already exists:
		} else if (instance != this) {
			//Destroy this, this enforces our singleton pattern so there can only be one instance of Hand.
			Destroy (gameObject);
		}
		//Set Deck to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
		DontDestroyOnLoad (gameObject);
	}

	void Start() {
		gameObject.GetComponent<Button>().onClick.AddListener (TaskOnClick);
		textUI = gameObject.transform.Find ("Text").gameObject;
		textUI.GetComponent<Text> ().text = "Match making...";
	}

	void TaskOnClick () {
		if (state) {
			GameManager.instance.emitTurnEnd ();
		}
	}

	public bool getState () {
		return state;
	}

	public void setState (bool _state) {
		state = _state;
		if (state) {
			textUI.GetComponent<Text> ().text = "Turn End";
		} else {
			textUI.GetComponent<Text> ().text = "Enemy Turn";
		}
	}

	public void enable () {
		state = true;
		textUI.GetComponent<Text> ().text = "Turn End";
	}

	public void disable () {
		state = false;
		textUI.GetComponent<Text> ().text = "Enemy Turn";
	}
}

