﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Quobject.SocketIoClientDotNet.Client;
using Newtonsoft.Json;

[System.Serializable]
public class CardData {
	public int id;
	public int type;
	public int health;
	public int damage;
	public int price;
	public string title;
	public string des;
	public int state;
};

[System.Serializable]
public class Result {
	public string gameId;
	public int requestId;
	public int userId;
	public int userState;
	public int attackerId;
	public int victimId;
	public int attacker;
	public int victim;
	public int price;
	public int heroHealth;
	public CardData[] cards;
	public CardData newCard;
	public CardData attackerCard;
	public CardData victimCard;
	public CardData playedCard;
	public CardData enemyPlayedCard;
};

public class GameManager : MonoBehaviour {
	public static GameManager instance = null;		//Allows other scripts to call functions from GameManager. 
	public string serverURL = "http://cardserver-elb-1756042256.us-west-2.elb.amazonaws.com:80";
	public Deck deckPrefab;
	public Card cardPrefab;
	public Dictionary<int, string> cardSprites = new Dictionary<int, string>();
	public GameObject gameOver;

	protected Socket socket = null;

	private List<CardData> cardDeckData = new List<CardData>();
	private Deck deck;											// prepared for card buffer

	private string gameId;

	private int userState;										// temp data using for initializing myHand in deck script
	private int userId;											// temp data using for initializing myHand in deck script
	private int newEnemyHealth;							// temp data using for thread safe enemy card instantiation.
	private int newMyHealth;								// temp data using for  thread safe our health update
	private int requestID = 0;								// request id counter
	private int handPrice = 1;								// start turn temp data for the price of my hero

	private bool deckInstantiable = false;			// Booleen for thread safe, deck instantiation.
	private bool cardInstantiable = false;			// Booleen for thread safe, card instantiation.
	private bool enemyNewCard = false;			// Booleen for thread safe, enemy card instantiation.
	private bool newTurn = false;						// Booleen for thread safe, enabling new turn init.
	private bool enemyHealthUpdate = false;	// Booleen for thread safe, enemy health update
	private bool myHealthUpdate = false;			// Booleen for thread safe, our health update
	private bool isGameOver = false;					// Booleen for thread safe, our health update
	private bool isAttack = false;							// Booleen for thread safe, play transform animation for attacking
	private bool attackFinish = false;					// Booleen for executing game play after play card attack animation
	private bool isCardAttack = false;					// Booleen for executing game play of card info after play card attack animation
	private bool isHeroAttack = false;					// Booleen for executing game play of hero info after play card attack animation

	private CardData newCardData;					// temp card data for instantiation
	private CardData playedCardData;				// temp card data for played card
	private CardData enemyPlayedCard;			// temp card data for enemy played card
	private CardData victimCardData = null;		// temp card data for victim card of attacking
	private CardData attackerCardData = null;	// temp card data for attacker card of attacking
	private Card attackerCard = null;					// temp card for attacker card of attacking
	private Card victimCard = null;						// temp card for victim card of attacking

	const int GAMEOVER = -1;
	const int ONLINE = 0;
	const int CONNECTED = 1;
	const int ENEMYTURN = 2;
	const int MYTURN = 3;
	const int WIN = 4;
	const int LOSE = 5;
	const int OFFLINE = -1;

	const int INDECK = 0;
	const int INHAND1 = 1;      // in hand, unable to play
	const int INHAND2 = 2;      // in hand, able to play
	const int ONTABLE1 = 3;     // on table, unable to attack
	const int ONTABLE2 = 4;     // on table, able to attack
	const int DESTROIED = -1;

	void Awake () {
		//Check if there is already an instance of GameManager
		if (instance == null) {
			//if not, set it to this.
			instance = this;
			//If instance already exists:
		} else if (instance != this) {
			//Destroy this, this enforces our singleton pattern so there can only be one instance of GameManager.
			Destroy (gameObject);
		}
		//Set GameManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
		DontDestroyOnLoad (gameObject);
		loadSprites ();

		DoOpen ();
	}

	void Destroy() {
		DoClose ();
	}

	void Update () {
		if (deckInstantiable) {
			deck = Instantiate (deckPrefab, Vector3.zero, Quaternion.identity) as Deck;
			if (newTurn == false)
				TurnBtn.instance.disable ();
		}
		if (cardInstantiable) {
			Card newCard = Instantiate (cardPrefab, Vector3.zero, Quaternion.identity) as Card;
			newCard.init (newCardData);
			Hand.instance.insert (newCard);
			cardInstantiable = false;
		}
		if (enemyNewCard) {
			Card newCard = Instantiate (cardPrefab, Vector3.zero, Quaternion.identity) as Card;
			newCard.init (enemyPlayedCard);
			EnemyTable.instance.insert (newCard.getId (), newCard);
			enemyNewCard = false;
		}
		if (newTurn) {
			TurnBtn.instance.setState (true);
			Hand.instance.attackInit ();
			MyHero.instance.updatePrice (handPrice);
			newTurn = false;
		}
		if (enemyHealthUpdate) {
			EnemyHero.instance.updateHealth (newEnemyHealth);
			enemyHealthUpdate = false;
		}
		if (myHealthUpdate) {			
			// animation of attacker card attacking our hero
			attackerCard.playAttack (MyHero.instance.transform.position);
			myHealthUpdate = false;
		}
		if (isGameOver) {
			gameOver.SetActive (true);

			if (userState == WIN) {
				Hand.instance.setState (WIN);
				gameOver.GetComponent<GameOver> ().updateText ("YOU WIN!");
			}
			else if (userState == LOSE) {
				Hand.instance.setState (LOSE);
				gameOver.GetComponent<GameOver> ().updateText ("YOU LOSE!");
			}

			isGameOver = false;
		}
		if (isAttack) {
			attackerCard.playAttack (victimCard.transform.position);
			isAttack = false;
		}
		if (isCardAttack && attackFinish) {
			afterPlayAttack ();
			isCardAttack = false;
			attackFinish = false;
		}
		if (isHeroAttack && attackFinish) {
			// update our hero health
			MyHero.instance.updateHealth (newMyHealth);
			isHeroAttack = false;
			attackFinish = false;
		}
	}

	// load files from disk and store the path into hash table
	void loadSprites () {
		cardSprites.Add (1, "scv");
		cardSprites.Add (2, "marine");
		cardSprites.Add (3, "reaper");
		cardSprites.Add (4, "marauder");
		cardSprites.Add (5, "hellion");
		cardSprites.Add (6, "ghost");
		cardSprites.Add (7, "viking");
		cardSprites.Add (8, "thor");
		cardSprites.Add (9, "siege_tank");
		cardSprites.Add (10, "medivac");
		cardSprites.Add (11, "nighthawk");
		cardSprites.Add (12, "banshee");
		cardSprites.Add (13, "battlecruiser");
	}

	/**
	 * Begin of RPC service
	 */
	void DoOpen() {
		if (socket == null) {
			socket = IO.Socket (serverURL);

			socket.On (Socket.EVENT_CONNECT, () => {
				socket.Emit ("connection");
			});
			/**
			 * 1. Connection
			 */
			socket.On ("connectionConfirmed", (data) => {				
				// deserialize received JSON data
				Result res = JsonUtility.FromJson<Result>(data.ToString());
				Debug.Log ("Connection Confirmed");

				userId = res.userId;
				userState = res.userState;

				Hand.instance.setId (userId);
				Hand.instance.setState (userState);
				Debug.Log ("User ID: " + res.userId);

				// Emit match making request
				socket.Emit ("matchMaking", requestID, userId);
				requestID++;
			});

			/**
	 		* 2. Match making
	 		*/
			socket.On ("matchMade", (data) => {				
				// deserialize received JSON data
				Result res = JsonUtility.FromJson<Result>(data.ToString());
				Debug.Log ("Match Made");

				if (res.userId == userId) {
					userState = res.userState;
					gameId = res.gameId;
					Hand.instance.setState (userState);

					// Emit Game Init request
					socket.Emit ("gameInit", requestID, userId, gameId);
					requestID++;
				}
			});

			/**
	 		* 3. Initialization
	 		*/
			socket.On ("initialized", (data) => {
				Debug.Log ("Game Initialized" + data);
				Result res = JsonUtility.FromJson<Result>(data.ToString());

				if (res.userId == userId) {
					userState = res.userState;
					Hand.instance.setState (userState);

					// initial hand cards
					foreach (CardData c in res.cards) {
						cardDeckData.Add (c);
					}						

					// state 3: my turn;
					if (userState == MYTURN) {
						newTurn = true;
					}
					deckInstantiable = true;
				}
			});

			/**
		     * 4. Start turn
		     */
			socket.On ("startTurn", (data) => {				
				Result res = JsonUtility.FromJson<Result>(data.ToString());

				if (res.userId == userId) {					
					userState = res.userState;
					Hand.instance.setState (userState);
					Hand.instance.setPrice (res.price);
					handPrice = res.price;

					newCardData = res.newCard;
					if (newCardData != null) {
						cardInstantiable = true;
					}

					newTurn = true;
				}
			});

			/**
		     * 5. Draw a card
		     */
			socket.On ("newCard", (data) => {				
				Result res = JsonUtility.FromJson<Result>(data.ToString());

				if (res.userId == userId) {
					userState = res.userState;
					Hand.instance.setState (userState);
					newCardData = res.newCard;

					if (newCardData != null) {
						cardInstantiable = true;
					}
				}
			});

			socket.On ("outOfCards", (data) => {				
				Result res = JsonUtility.FromJson<Result>(data.ToString());
				Debug.Log ("Out of Cards!");

				if (res.userId == userId) {
					userState = res.userState;
					Hand.instance.setState (userState);
					Debug.Log ("User state: " + res.userState);
				}
			});

			/**
	 		* 6. Play a Card
	 		*/
			socket.On ("updateTable", (data) => {				
				Result res = JsonUtility.FromJson<Result>(data.ToString());
				Debug.Log ("Update their Table " + data);

				if (res.userId == userId) {
					enemyPlayedCard = res.enemyPlayedCard;
					Debug.Log ("Enemy played card id: " + enemyPlayedCard.id + ", type: " + enemyPlayedCard.type +
						", health: " + enemyPlayedCard.health + ", damage: " + enemyPlayedCard.damage);
					enemyNewCard = true;
				}
			});

			/**
	 		* 7. Attack a Hero
	 		*/
			socket.On ("updateHero", (data) => {
				Result res = JsonUtility.FromJson<Result>(data.ToString());

				if (userId == res.victim) {
					newMyHealth = res.heroHealth;
					attackerCard = EnemyTable.instance.getCardById (res.attackerId);

					if (attackerCard == null) {
						Debug.Log ("Attack card " + res.attackerId + " couldn't be found");
					}
					myHealthUpdate = true;
					isHeroAttack = true;
					Debug.Log ("We are damaged by card " + res.attackerId);
				}
//				else if (userId == res.attacker) {
//					if (EnemyHero.instance.getHealth() != res.heroHealth) {
//						newEnemyHealth = res.heroHealth;
//						enemyHealthUpdate = true;
//						Debug.Log ("Enemy health corrected");
//					}
//				}
			});
			/**
			* 8. Update cards
			*/
			socket.On ("updateCards", (data) => {
				Result res = JsonUtility.FromJson<Result>(data.ToString());

				// update the health of our victim card
				if (userId == res.victim) {	
					victimCardData = res.victimCard;
					attackerCardData = res.attackerCard;
					attackerCard = EnemyTable.instance.getCardById (attackerCardData.id);
					victimCard = Hand.instance.getCardById (victimCardData.id);

					if (attackerCard == null) {
						Debug.Log ("Attack card " + res.attackerCard.id + ", type " + res.attackerCard.type + " couldn't be found");
					}
					if (victimCard == null) {
						Debug.Log ("Attack card " + res.attackerCard.id + ", type " + res.attackerCard.type + " couldn't be found");
					}

					isAttack = true;
					isCardAttack = true;
				}
			});		

			/**
			* 9. Game Over
			*/
			socket.On ("gameOver", (data) => {
				Result res = JsonUtility.FromJson<Result>(data.ToString());
				Debug.Log ("Game Over, player state: " + res.userState);

				if (userId == res.userId) {
					userState = res.userState;
					isGameOver = true;
				}
			});
		}
	}

	// close socket connection
	void DoClose() {
		if (socket != null) {
			socket.Emit ("disconnect", userId);
			socket.Disconnect ();
			socket = null;
		}
	}

	public List<CardData> getCardDeckData () {
		return cardDeckData;
	}

	public void setDeckInstantiable (bool _instantiable) {
		deckInstantiable = _instantiable;
	}

	public void setCardInstantiable (bool _instantiable) {
		cardInstantiable = _instantiable;
	}

	public int getMyId () {
		return userId;
	}

	public int getMyState () {
		return userState;
	}

	public void emitTurnEnd () {
		socket.Emit ("turnEnd", requestID, userId);
		requestID++;
		Hand.instance.setState (ENEMYTURN);
		TurnBtn.instance.disable ();
	}

	public void emitPlayCard (int cardID, int _price) {
		socket.Emit ("playCard", requestID, userId, cardID);
		requestID++;
		Hand.instance.updatePrice (_price);

		if (Hand.instance.isEndTurn ()) {
			emitTurnEnd ();
		}
	}

	public void emitAttackHero (int id, int val) {
		socket.Emit ("attackHero", requestID, userId, id);
		requestID++;
		Hand.instance.reduceAttackableCount ();

		if (Hand.instance.isEndTurn ()) {
			emitTurnEnd ();
		}
	}

	public void emitAttackCard (int _attackId, int _victimId) {
		socket.Emit ("attackCard", requestID, userId, _attackId, _victimId);
		requestID++;
		Hand.instance.reduceAttackableCount ();

		if (Hand.instance.isEndTurn ()) {
			emitTurnEnd ();
		}
	}

	// update health data of cards after attacking animation
	public void afterPlayAttack () {
		if (victimCardData.health <= 0) {
			Hand.instance.removeCardById (victimCardData.id);
		} else {						
			victimCard.updateHealth (victimCardData.health);
		}
		Debug.Log ("Our card " + victimCardData.id + " is under attack, new health: " + victimCardData.health);

		// update the health of enemy attacker card
		if (attackerCardData.health <= 0) {
			EnemyTable.instance.removeCardById (attackerCardData.id);
		} else {						
			attackerCard.updateHealth (attackerCardData.health);
		}
		Debug.Log ("The enemy card " + attackerCardData.id + " attacked us, new health: " + victimCardData.health);
	}

	public void setAttackFinish () {
		attackFinish = true;
	}
}
