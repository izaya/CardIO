﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Lobby : MonoBehaviour {
	void Start () {
		gameObject.GetComponent<Button>().onClick.AddListener (TaskOnClick);
	}
	
	void TaskOnClick () {
		Application.LoadLevel("Game");
	}
}

