﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MyHero : MonoBehaviour {
	public static MyHero instance = null;
	private int health = 20;
	private GameObject priceUI = null;
	private GameObject healthUI = null;

	void Awake () {		
		//Check if there is already an instance of Hand.
		if (instance == null) {
			//if not, set it to this.
			instance = this;
			//If instance already exists:
		} else if (instance != this) {
			//Destroy this, this enforces our singleton pattern so there can only be one instance of Hand.
			Destroy (gameObject);
		}
		//Set Deck to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
		DontDestroyOnLoad (gameObject);
	}

	void Start () {
		priceUI = gameObject.transform.Find ("Price").gameObject;
		healthUI = gameObject.transform.Find ("Health").gameObject;
		priceUI.GetComponent<Text> ().text = "1";
		healthUI.GetComponent<Text> ().text = "20";
	}
	
	public int getHealth () {
		return health;
	}

	public void sabotage (int val) {
		health -= val;
	}

	public void updatePrice (int price) {
		priceUI.GetComponent<Text> ().text = price.ToString();
	}

	public void updateHealth (int health) {
		healthUI.GetComponent<Text> ().text = health.ToString();
	}
}
