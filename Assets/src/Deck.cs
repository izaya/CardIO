﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Deck : MonoBehaviour {
	public static Deck instance = null;
	public Card cardPrefab;

	private List<CardData> cardDeckData = new List<CardData>();	// only used for receive data from network
	private List<Card> cardDeck = new List<Card>();		// initial hand cards

	void Awake () {
		//Check if there is already an instance of Deck.
		if (instance == null) {
			//if not, set it to this.
			instance = this;
			GameManager.instance.setDeckInstantiable (false);
			//If instance already exists:
		} else if (instance != this) {
			//Destroy this, this enforces our singleton pattern so there can only be one instance of Deck.
			Destroy (gameObject);
		}
		//Set Deck to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
		DontDestroyOnLoad (gameObject);

		cardDeckData = GameManager.instance.getCardDeckData ();

		for (int i = 0; i < cardDeckData.Count; i++) {
			Card newCard = Instantiate (cardPrefab, Vector3.zero, Quaternion.identity) as Card;

			if (newCard == null) {
				Debug.Log ("Fail to instantiate new card");
				continue;
			} else {
				newCard.init (cardDeckData[i]);
				cardDeck.Add (newCard);
			}
		}

		// put card deck data into hand
		Hand.instance.init (GameManager.instance.getMyId (), GameManager.instance.getMyState (), cardDeck);
	}
}

