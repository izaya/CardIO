﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyTable : MonoBehaviour {
	public static EnemyTable instance = null;
	private Dictionary<int, Card> cards = new Dictionary<int, Card>();

	void Awake () {
		//Check if there is already an instance of Hand.
		if (instance == null) {
			//if not, set it to this.
			instance = this;
			//If instance already exists:
		} else if (instance != this) {
			//Destroy this, this enforces our singleton pattern so there can only be one instance of Hand.
			Destroy (gameObject);
		}
		//Set Deck to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
		DontDestroyOnLoad (gameObject);
	}

	public void insert (int _id, Card item) {
		cards.Add (_id, item);
		item.transform.SetParent (instance.transform);
		item.setState (3);	
		item.gameObject.GetComponent<Touch>().setActivate ();
	}

	// Remove and destroy this card by ID
	public bool removeCardById (int _id) {
		Card thisCard = cards[_id];

		if (cards.Remove (_id)) {
			thisCard.updateHealth (-1);
			Debug.Log ("Removed card " + _id + " from enemy table");
			return true;
		} else {
			Debug.Log ("Fail to remove card  " + _id + " from enemy table");
			return false;
		}			
	}

	public Card getCardById (int _id) {
		if (cards.ContainsKey (_id)) {
			return cards[_id];
		}
		Debug.Log ("Fail to find card " + _id + " from Enemy Table played cards");
		return null;
	}

	public bool containsKey (int _key) {
		return cards.ContainsKey (_key);
	}
}
