﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Touch : MonoBehaviour, IDropHandler {
	private Card thisCard;
	private int thisDamage;
	private int thisId;
	private bool activate;

	void Start () {
		thisCard = gameObject.GetComponent<Card> ();

		if (thisCard == null) {
			Debug.Log ("FAIL to get card component of touch class");
		} else {
			thisId = thisCard.getId ();
			thisDamage = thisCard.getDamage ();
			activate = EnemyTable.instance.containsKey (thisId);
		}
	}

	public void OnDrop(PointerEventData eventData) {
		if (!activate) {
			return;
		}
		Attack attack_comp = eventData.pointerDrag.GetComponent<Attack>();

		if(attack_comp != null) {
			Card attackCard = attack_comp.gameObject.GetComponent<Card> ();

			if (attackCard != null) {
				int damage = attackCard.sabotage (thisDamage);
				thisCard.sabotage (damage);
				GameManager.instance.emitAttackCard (attackCard.getId(), thisId);
				Debug.Log ("Card " + thisId + " is under attack");
			}
		}
	}

	public void setActivate () {
		activate = true;
	}
}

