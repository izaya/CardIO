﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Card : MonoBehaviour {
	private int state;
	private int id;
	private int type;
	private int health;
	private int damage;
	private int price;
	private string title;
	private string des;
	private GameObject titleUI;
	private GameObject desUI;
	private GameObject healthUI;
	private GameObject damageUI;
	private GameObject priceUI;
	private GameObject imageUI;
	private GameObject frameUI;

	private bool isUpdateHealth;
	private bool isDestroy;
	private bool isPlayAttack;
	private bool isReach;

	private Vector3 target;
	private Vector3 origin;
	public float smoothing = 7f;

	const int INDECK = 0;
	const int INHAND1 = 1;      // in hand, unable to play
	const int INHAND2 = 2;      // in hand, able to play
	const int ONTABLE1 = 3;     // on table, unable to attack
	const int ONTABLE2 = 4;     // on table, able to attack
	const int DESTROIED = -1;

	public Vector3 Target {
		get {
			return target;
		}
		set {
			target = value;
			StartCoroutine("Movement", target);
		}
	}		

	void Awake() {
		desUI = gameObject.transform.GetChild(0).Find ("Description").gameObject;
		imageUI = gameObject.transform.GetChild(0).Find ("Image").gameObject;
		titleUI = gameObject.transform.GetChild(0).Find ("Title").gameObject;
		healthUI = gameObject.transform.GetChild(0).Find ("Health").gameObject;
		damageUI = gameObject.transform.GetChild(0).Find ("Damage").gameObject;
		priceUI = gameObject.transform.GetChild(0).Find ("Price").gameObject;
		frameUI = gameObject.transform.GetChild (0).gameObject;
		isUpdateHealth = false;
		isDestroy = false;
		isReach = false;
		isPlayAttack = false;
	}

	void Update () {
		if (isUpdateHealth) {
			isUpdateHealth = false;
			healthUI.GetComponent<Text> ().text = health.ToString();
		}
		if (isDestroy) {
			isDestroy = false;
			Destroy (gameObject);
		}
		if (isPlayAttack && isReach) {
			Target = origin;
			isPlayAttack = false;
			isReach = false;
		}
		if (isReach && !isPlayAttack) {
			GameManager.instance.setAttackFinish ();
			isReach = false;
		}
	}

	public void init (CardData card_data) {
		id = card_data.id;
		type = card_data.type;
		health = card_data.health;
		damage = card_data.damage;
		price = card_data.price;
		title = card_data.title;
		des = card_data.des;
		state = card_data.state;

		titleUI.GetComponent<Text> ().text = title;
		desUI.GetComponent<Text> ().text = des;
		healthUI.GetComponent<Text> ().text = health.ToString();
		damageUI.GetComponent<Text> ().text = damage.ToString();
		priceUI.GetComponent<Text> ().text = price.ToString();
		imageUI.GetComponent<Image> ().sprite = Resources.Load<Sprite> ("sc2/terran/" + GameManager.instance.cardSprites [type]);
	}

	public int getType () {
		return type;
	}

	public int getId () {
		return id;
	}

	public int getHealth () {
		return health;
	}

	public int getDamage () {
		return damage;
	}

	public int getState () {
		return state;
	}

	public int getPrice () {
		return price;
	}

	public string getTitle () {
		return title;
	}

	public void setState (int _state) {
		if (state == DESTROIED) {
			Debug.Log ("Card id " + id + " , type " + type + " has been set to -1");
			Destroy (gameObject);
			return;
		}
		state = _state;
	}

	public void hightlight () {
		if (state == DESTROIED) {
			Debug.Log ("Card has been set to -1");
			return;
		}
		frameUI.GetComponent<Image> ().color = new Color32 (0, 255, 0, 255);
		state = INHAND2;
	}

	public void disable () {
		if (state == DESTROIED) {
			Debug.Log ("Card has been set to -1");
			return;
		}
		frameUI.GetComponent<Image> ().color = new Color32 (255, 255, 255, 255);
		state = INHAND1;
	}

	public void active () {
		if (state == DESTROIED) {
			Debug.Log ("Card has been set to -1");
			return;
		}
		frameUI.GetComponent<Image> ().color = new Color32 (255, 255, 0, 255);
		state = ONTABLE2;
	}

	public void deactive () {
		if (state == DESTROIED) {
			Debug.Log ("Card has been set to -1");
			return;
		}
		frameUI.GetComponent<Image> ().color = new Color32 (255, 255, 255, 255);
		state = ONTABLE1;
	}

	public int sabotage (int val) {
		health -= val;

		if (health <= 0) {
			Debug.Log ("Card " + id + " is destroyed");
			state = DESTROIED;
			gameObject.SetActive (false);
		} else {
			healthUI.GetComponent<Text> ().text = health.ToString();
		}

		return damage;
	}

	public void updateHealth (int val) {
		if (state == DESTROIED) {
			Debug.Log ("Card has been set to -1");
			return;
		}
		health = val;

		if (health <= 0) {
			Debug.Log ("Card " + id + " is destroyed");
			state = DESTROIED;
			isDestroy = true;
		} else {
			isUpdateHealth = true;
		}
	}

	IEnumerator Movement (Vector3 target) {
		while(Vector3.Distance(transform.position, target) > 0.05f) {
			transform.position = Vector3.Lerp(transform.position, target, smoothing * Time.deltaTime);		
			yield return null;
		}
		isReach = true;
		yield return null;
	}

	public void playAttack (Vector3 _target) {
		isPlayAttack = true;
		origin = transform.position;
		Target = _target;
	}
}
