CardIO
======

CardIO is an online multiplayer card game made with Unity and
[Socket.IO](https://socket.io/). It's purely for engineering learning purpose as
there are not many online tutorials about cloud server of making a card game using
Socket.io. This repository only has code on client side. The server code is held in
another repository [CardServer](https://gitlab.com/izaya/CardServer) written in
Node.js with API documentations.  
For more information about Socket.io client APIs, check out [https://socket.io/docs/client-api/](https://socket.io/docs/client-api/)

### Dependencies

Unity v5.6.1  
socket.io-unity <https://github.com/floatinghotpot/socket.io-unity>  
(You can drag those DLL files into your Asset folder and include them in your
Unity files; Make sure to set the .NET version to be 2.0 when building the game)

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
using Quobject.SocketIoClientDotNet.Client;
using Newtonsoft.Json;
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

### Gameplay

The gameplay mechanic is just like HearthStone. Each card has health and
damage values and the players could use their cards played on the table to attack
the opponents’ cards or the hero by dragging your attacking card onto the enemy card or hero. For each turn, the player
has a maximum resources to play the cards with the total price less than that
resource value. The game would automatically end your turn if there is no card available to play. If the hero health is less than 0 or the player quit the game,
the other player would win.

Here in this example screenshot, the green cards are the cards you can play onto the table from your hand which your enemy couldn't see.
The status would update as you can only play cards with the total resources less than the maximum resource you can play for this turn.
The yellow cards are those you can use to attack.
<img src="/myHS.png">